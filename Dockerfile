FROM node:10-alpine3.10 as front-build
LABEL authors="veiko.laats@energia.ee"
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
ENV PATH /usr/src/app/node_modules/.bin:$PATH
COPY package*.json /usr/src/app/
RUN npm ci
COPY . /usr/src/app
RUN npm run test && npm run build

# Arguments for versioning
ARG BUILD_NUMBER=no_build_number_set
ARG BRANCH_NAME=no_branch_name_set
RUN echo -e "{\n\t\"buildNumber\": \"$BRANCH_NAME.$BUILD_NUMBER\"\n}" > /usr/src/app/dist/meta.json

FROM nginx:1.17.8-alpine

COPY --from=front-build /usr/src/app/dist /usr/share/nginx/html
RUN chmod -R 644 /usr/share/nginx/html
RUN chmod -R +X /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
COPY conf/nginx.conf /etc/nginx/conf.d
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
