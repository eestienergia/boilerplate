import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import Navbar from './shared/components/Navbar';
import './App.scss';

import modules from './modules';

const App = () => (
  <Router>
    <Navbar modules={modules} />
    {modules.map((module) => (
      <Route {...module.routeProps} key={module.name} />
    ))}
  </Router>
);

export default App;
