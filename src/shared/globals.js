const API_URL = 'http://jsonplaceholder.typicode.com';

const API = {
  albums: `${API_URL}/albums`,
  users: `${API_URL}/users`,
};

export default API;
