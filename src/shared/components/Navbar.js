import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import classes from 'classnames';

const Navbar = ({ modules }) => {
  const [currentTab, setCurrentTab] = useState('Albums');

  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <div className="collapse navbar-collapse">
        <ul className="navbar-nav">
          {modules.map((module) => (
            <li
              key={module.name}
              className={classes('nav-item', {
                active: currentTab === module.name,
              })}
            >
              <Link
                to={module.routeProps.path}
                className="nav-link"
                onClick={() => setCurrentTab(module.name)}
              >
                {module.name}
              </Link>
            </li>
          ))}
        </ul>
      </div>
    </nav>
  );
};

Navbar.propTypes = {
  modules: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default Navbar;
