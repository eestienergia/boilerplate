import { combineReducers } from 'redux';
import albums from './Album/reducer';

const rootReducer = combineReducers({
  albums,
});

export default rootReducer;
