import thunk from 'redux-thunk';
import configureMockStore from 'redux-mock-store';
import { fetchAlbums } from './actions';
import { RECEIVE_ALBUMS, REQUEST_ALBUMS } from './reducer';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('album actions', () => {
  let store;

  beforeEach(() => {
    store = mockStore();
  });

  it('should call expected actions when status ok', () => {
    window.fetch = jest.fn().mockImplementation(
      () =>
        // eslint-disable-next-line implicit-arrow-linebreak
        Promise.resolve({ ok: true, status: 200, json: () => [] })
      // eslint-disable-next-line function-paren-newline
    );
    store.dispatch(fetchAlbums()).then(() => {
      const storeActions = store.getActions();
      expect(storeActions[0].type).toEqual(REQUEST_ALBUMS);
      expect(storeActions[1].type).toEqual(RECEIVE_ALBUMS);
      expect(storeActions[1].albums).toEqual([]);
    });
  });

  it('should call expected actions when status not Ok', () => {
    window.fetch = jest
      .fn()
      // eslint-disable-next-line prefer-promise-reject-errors
      .mockImplementation(() => Promise.reject({ ok: false, status: 500 }));
    store
      .dispatch(fetchAlbums())
      .then(() => {
        const storeActions = store.getActions();
        expect(storeActions.length).toBe(1);
        expect(storeActions[0].type).toBe(REQUEST_ALBUMS);
      })
      .catch(() => {
        // Test error cases here
      });
  });
});
