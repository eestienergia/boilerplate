import React from 'react';
import PropTypes from 'prop-types';
import ListItem from './ListItem';

const List = ({ albums, onAlbumClick, selectedAlbum }) => (
  <div className="list-group fixed-height">
    {albums.map((album) => (
      <ListItem
        album={album}
        key={album.id}
        onAlbumClick={onAlbumClick}
        selectedAlbum={selectedAlbum}
      />
    ))}
  </div>
);

List.propTypes = {
  albums: PropTypes.arrayOf(PropTypes.object).isRequired,
  onAlbumClick: PropTypes.func.isRequired,
  selectedAlbum: PropTypes.shape({
    title: PropTypes.string,
    id: PropTypes.number,
  }),
};

List.defaultProps = {
  selectedAlbum: {},
};

export default List;
