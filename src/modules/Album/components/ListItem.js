import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classes from 'classnames';

class ListItem extends Component {
  render() {
    const {
      album: { title, id },
      album,
      onAlbumClick,
      selectedAlbum,
    } = this.props;

    return (
      <button
        type="button"
        onClick={() => onAlbumClick(album)}
        className={classes(
          'list-group-item list-group-item-action flex-column align-items-start',
          {
            active: id === selectedAlbum.id,
          }
        )}
      >
        <span className="d-flex align-items-center">
          <ul className="album-metainfo-list">
            <li className="album-metainfo-list__item album-title">{title}</li>
          </ul>
        </span>
      </button>
    );
  }
}

ListItem.propTypes = {
  selectedAlbum: PropTypes.shape({
    title: PropTypes.string,
    id: PropTypes.number,
  }).isRequired,
  album: PropTypes.shape({
    title: PropTypes.string,
    id: PropTypes.number,
  }).isRequired,
  onAlbumClick: PropTypes.func.isRequired,
};

export default ListItem;
