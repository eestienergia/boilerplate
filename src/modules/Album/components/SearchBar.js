import React from 'react';
import PropTypes from 'prop-types';

const SearchBar = ({ onSearchTermChange }) => (
  <div className="input-group mb-2">
    <input
      type="text"
      className="form-control"
      onChange={(event) => onSearchTermChange(event.target.value)}
      placeholder="Search album..."
    />
  </div>
);

SearchBar.propTypes = {
  onSearchTermChange: PropTypes.func.isRequired,
};

export default SearchBar;
