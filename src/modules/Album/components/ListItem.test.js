import React from 'react';
import { shallow } from 'enzyme';
import ListItem from './ListItem';

const props = {
  album: {
    title: 'Demo 1',
    id: 1,
  },
  onAlbumClick: jest.fn(),
  selectedAlbum: {
    title: 'Demo 0',
    id: 0,
  },
};

describe('ListItem', () => {
  const wrapper = shallow(<ListItem {...props} />);

  it('should call onClick', () => {
    const buttonElem = wrapper.find('button');

    buttonElem.simulate('click');

    expect(props.onAlbumClick.mock.calls[0][0].title).toEqual(
      props.album.title
    );
    expect(props.onAlbumClick.mock.calls[0][0].id).toEqual(props.album.id);
  });

  it('should have active class', () => {
    const newSelectedAlbum = {
      title: 'Demo 1',
      id: 1,
    };
    const buttonElem = wrapper.find('button');

    expect(buttonElem.hasClass('active')).toBe(false);
    expect(buttonElem.prop('className')).toEqual(
      'list-group-item list-group-item-action flex-column align-items-start'
    );
    wrapper.setProps({ selectedAlbum: newSelectedAlbum });
    expect(wrapper.find('button').hasClass('active')).toBe(true);
  });
});
