import React from 'react';
import PropTypes from 'prop-types';

const Details = ({ selectedAlbum }) => {
  const { title } = selectedAlbum;
  return (
    <div className="card">
      <div className="card-body">
        <h2 className="card-title">{title}</h2>
      </div>
    </div>
  );
};

Details.propTypes = {
  selectedAlbum: PropTypes.shape({
    title: PropTypes.string,
  }),
};

Details.defaultProps = {
  selectedAlbum: {},
};

export default Details;
