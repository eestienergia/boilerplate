import React from 'react';
import { shallow } from 'enzyme';
import Details from './Details';

const props = {
  selectedAlbum: {
    title: '',
  },
};

describe('Details', () => {
  const wrapper = shallow(<Details {...props} />);

  it('should render itself', () => {
    expect(wrapper.find('.card').length).toBe(1);
    expect(wrapper.find('.card-body').length).toBe(1);
  });

  it('should render title empty initially', () => {
    expect(wrapper.find('h2.card-title').text()).toEqual('');
  });

  it('should render title when its provided', () => {
    const mockTitle = {
      title: 'Demo title',
    };
    wrapper.setProps({ selectedAlbum: mockTitle });

    expect(wrapper.find('h2.card-title').text()).toEqual(mockTitle.title);
  });
});
