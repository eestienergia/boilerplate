import React from 'react';
import { shallow } from 'enzyme';
import List from './List';

const props = {
  albums: [],
  onAlbumClick: jest.fn(),
  selectedAlbum: {
    title: '',
    id: null,
  },
};

describe('List', () => {
  const wrapper = shallow(<List {...props} />);

  it('should render something', () => {
    expect(wrapper.find('.list-group').children().length).toBe(0);
  });

  it('should render ListItem when albums provided', () => {
    const mockAlbums = [
      { userId: 1, id: 1, title: 'quidem molestiae enim 1' },
      { userId: 2, id: 2, title: 'quidem molestiae enim 2' },
    ];
    wrapper.setProps({ albums: mockAlbums });

    mockAlbums.forEach((album, index) => {
      const listItem = wrapper.find('ListItem').at(index);

      expect(listItem.prop('album')).toEqual(album);
      expect(listItem.key()).toEqual(`${album.id}`);
      expect(listItem.prop('onAlbumClick')).toEqual(props.onAlbumClick);
      expect(listItem.prop('selectedAlbum')).toEqual(props.selectedAlbum);
    });
  });
});
