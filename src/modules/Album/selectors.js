import { createSelector } from 'reselect';

const allAlbums = (state) => state.albums.list;
const searchTerm = (state) => state.albums.searchTerm;

const getAlbumbsBySearchTerm = createSelector(
  [allAlbums, searchTerm],
  (albums, term) => {
    if (!term) {
      return albums;
    }
    return albums.filter(
      (album) => album.title.toLowerCase().indexOf(term.toLowerCase()) >= 0
    );
  }
);

export default getAlbumbsBySearchTerm;
