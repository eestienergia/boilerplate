import { combineReducers } from 'redux';

export const ALBUM_SELECTED = 'albums/ALBUM_SELECTED';
export const REQUEST_ALBUMS = 'albums/REQUEST_ALBUMS';
export const RECEIVE_ALBUMS = 'albums/RECEIVE_ALBUMS';
export const SEARCH_TERM = 'albums/SEARCH_TERM';

function isLoading(state = false, action) {
  switch (action.type) {
    case REQUEST_ALBUMS:
      return true;
    case RECEIVE_ALBUMS:
      return false;
    default:
      return state;
  }
}

function list(state = [], action) {
  switch (action.type) {
    case RECEIVE_ALBUMS:
      return action.albums;
    default:
      return state;
  }
}

function selected(state = null, action) {
  switch (action.type) {
    case ALBUM_SELECTED:
      return action.album;
    default:
      return state;
  }
}

function searchTerm(state = null, action) {
  switch (action.type) {
    case SEARCH_TERM:
      return action.searchTerm;
    case REQUEST_ALBUMS:
      return null;
    default:
      return state;
  }
}

export default combineReducers({
  isLoading,
  list,
  selected,
  searchTerm,
});
