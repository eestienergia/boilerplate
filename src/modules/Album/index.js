import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { selectAlbum, fetchAlbums, setSearchTerm } from './actions';
import getAlbumbsBySearchTerm from './selectors';

import Details from './components/Details';
import List from './components/List';
import SearchBar from './components/SearchBar';

class Album extends Component {
  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(fetchAlbums());
  }

  onAlbumClick = (album) => {
    const { dispatch } = this.props;
    dispatch(selectAlbum(album));
  };

  onSearchTermChange = (term) => {
    const { dispatch } = this.props;
    dispatch(setSearchTerm(term));
  };

  render() {
    const { albums, selectedAlbum } = this.props;
    return (
      <div className="wrapper">
        <div className="row">
          <div className="col-sm-12">
            <h1 className="text-center">Albums 2</h1>
          </div>
        </div>
        <div className="row main__content">
          <div className="col-sm-6">
            {albums[0] && (
              <Details selectedAlbum={selectedAlbum || albums[0]} />
            )}
          </div>
          <div className="col-sm-6">
            <SearchBar
              onSearchTermChange={(term) => this.onSearchTermChange(term)}
            />
            <List
              albums={albums}
              onAlbumClick={(album) => this.onAlbumClick(album)}
              selectedAlbum={selectedAlbum || albums[0]}
            />
          </div>
        </div>
      </div>
    );
  }
}

Album.propTypes = {
  albums: PropTypes.arrayOf(PropTypes.object),
  selectedAlbum: PropTypes.shape({
    title: PropTypes.string,
  }),
  dispatch: PropTypes.func.isRequired,
};

Album.defaultProps = {
  albums: [],
  selectedAlbum: {},
};

function mapStateToProps(state) {
  return {
    albums: getAlbumbsBySearchTerm(state),
    selectedAlbum: state.albums.selected,
  };
}

export default {
  routeProps: {
    path: '/',
    exact: true,
    component: connect(mapStateToProps)(Album),
  },
  name: 'Albums',
};
