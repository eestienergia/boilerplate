import API from '../../shared/globals';
import {
  REQUEST_ALBUMS,
  RECEIVE_ALBUMS,
  ALBUM_SELECTED,
  SEARCH_TERM,
} from './reducer';

function requestAlbums() {
  return {
    type: REQUEST_ALBUMS,
  };
}

function receiveAlbums(albums) {
  return {
    type: RECEIVE_ALBUMS,
    albums,
  };
}

export function selectAlbum(album) {
  return {
    type: ALBUM_SELECTED,
    album,
  };
}

export function setSearchTerm(searchTerm) {
  return {
    type: SEARCH_TERM,
    searchTerm,
  };
}

export const fetchAlbums = () => async (dispatch) => {
  dispatch(requestAlbums());
  try {
    const response = await fetch(`${API.albums}`);
    if (!response.ok) {
      throw { status: response.status };
    }
    const data = await response.json();
    dispatch(receiveAlbums(data));
  } catch (error) {
    // eslint-disable-next-line no-console
    // console.log(error);
  }
};
