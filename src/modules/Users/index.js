import React from 'react';
import List from './components/List';
import { UsersProvider } from './context';

const Users = () => (
  <div className="wrapper">
    <div className="row">
      <div className="col-sm-12">
        <h1 className="text-center">Users</h1>
      </div>
    </div>
    <div className="row main__content">
      <div className="col-sm-12">
        <UsersProvider>
          <List />
        </UsersProvider>
      </div>
    </div>
  </div>
);

export default {
  routeProps: {
    path: '/users',
    exact: true,
    component: Users,
  },
  name: 'Users',
};
