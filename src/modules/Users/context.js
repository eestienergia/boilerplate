import React, { useReducer, useEffect } from 'react';
import PropTypes from 'prop-types';

import API from '../../shared/globals';
import reducer, { initialState } from './reducer';
import { requestUsers, receiveUsers } from './actions';

const UsersStateContext = React.createContext();

const UsersProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  useEffect(async () => {
    dispatch(requestUsers());
    try {
      const response = await fetch(`${API.users}`);
      if (!response.ok) {
        throw { status: response.status };
      }
      const data = await response.json();
      dispatch(receiveUsers(data));
    } catch (error) {
      // eslint-disable-next-line no-console
      console.log(error);
    }
  }, []);

  return (
    <UsersStateContext.Provider value={state}>
      {children}
    </UsersStateContext.Provider>
  );
};

function useUsersState() {
  const context = React.useContext(UsersStateContext);
  if (context === undefined) {
    throw new Error('useUsersState must be used within a UsersProvider');
  }
  return context;
}

UsersProvider.propTypes = {
  children: PropTypes.node.isRequired,
};

export { UsersProvider, useUsersState };
