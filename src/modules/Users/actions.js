import { REQUEST_USERS, RECEIVE_USERS } from './reducer';

export function requestUsers() {
  return {
    type: REQUEST_USERS,
  };
}

export function receiveUsers(users) {
  return {
    type: RECEIVE_USERS,
    users,
  };
}
