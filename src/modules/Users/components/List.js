import React from 'react';
import { useUsersState } from '../context';

const List = () => {
  const { users, isLoading } = useUsersState();

  if (isLoading) {
    return <div>Loading...</div>;
  }

  return (
    <ul className="list-group">
      {users.map((user) => (
        <li
          key={user.id}
          className="list-group-item list-group-item-action flex-column align-items-start"
        >
          <div className="d-flex w-100">
            <h5 className="mb-1">{user.name}</h5>
          </div>
          <p className="mb-1">{`Website: ${user.website}`}</p>
          <p className="mb-0">{`E-mail: ${user.email}`}</p>
        </li>
      ))}
    </ul>
  );
};

export default List;
