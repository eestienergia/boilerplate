export const REQUEST_USERS = 'users/REQUEST_USERS';
export const RECEIVE_USERS = 'users/RECEIVE_USERS';

export const initialState = {
  users: [],
  isLoading: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case REQUEST_USERS:
      return {
        ...state,
        isLoading: true,
      };
    case RECEIVE_USERS:
      return {
        ...state,
        isLoading: false,
        users: action.users,
      };
    default:
      return state;
  }
};
